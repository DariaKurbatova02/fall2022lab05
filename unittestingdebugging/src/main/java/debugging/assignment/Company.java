package debugging.assignment;

public class Company {
    private Employee[] employees;

    public Company(Employee[] employees) {
        Employee[] emp = new Employee[employees.length];
        for (int i=0; i<employees.length; i++){
            emp[i] = new Employee(employees[i]);
        }
        this.employees = emp;
    }
    public Company(Company other){
        this(other.employees);
    }    

    /**
     * Exercise: Fill in the JavaDocs comment to describe what this method is supposed to do!
     * @param name
     * @return
     */
    public boolean doesWorkFor(String name) {
        for (Employee e : employees) {
            if (e.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Company)) {
            return false;
        }
        
        Company other = (Company) o;
        for (int i = 0; i < other.employees.length; i++) {
            if (!other.employees[i].equals(this.employees[i])) {
                return false;
            }
        }
        return true;
    }

}
